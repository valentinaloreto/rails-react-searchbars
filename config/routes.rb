Rails.application.routes.draw do

  root to: "pages#home"
  get 'countries', to: 'pages#countries'

  # animals index will return a view; countries index will return a json
  resources :animals, only: :index
  # resources :countries, only: :index

  namespace :api do
    namespace :v1 do
     resources :countries, only: [:index]
    end
  end

end
