class Countries extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      countries: [],
    };
    this.searchByName = this.searchByName.bind(this);

  }

  componentDidMount(){
    fetch('/api/v1/countries.json')
      .then((response) => {  return response.json()})
      .then((data) => {this.setState({ countries: data }) });
  }

  searchByName() {
    let testme = document.querySelector('#testme')
    // alert(testme.value);
    // alert('/api/v1/countries.json?name=' + testme.value )
    fetch('/api/v1/countries.json?name=' + testme.value )
      .then((response) => {  return response.json()})
      .then((data) => {this.setState({ countries: data }) });
  }


  // **************************************************

  render () {
    var countries = this.state.countries.map ( (country)=> {
      return(
        <li key={country.id}>
          {country.name}
        </li>
      )
    })

    return(
      <div>


        <input id="testme" type="text" name="name" placeholder="Search by name..."></input>
        <button onClick={ this.searchByName }>Go</button>

        <ul>
          {countries}
        </ul>
      </div>
    )
  }
}
