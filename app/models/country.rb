class Country < ApplicationRecord

  def self.find_by_name(arg)
    where('name LIKE ?', "%#{arg}%")
  end

end
