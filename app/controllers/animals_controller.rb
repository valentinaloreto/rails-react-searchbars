class AnimalsController < ApplicationController
  def index

    if params[:name]

      @animals = Animal.find_by_name(params[:name])

    else
      @animals = Animal.all
    end
  end

  private

  def animal_params
    params.require(:animal).permit(:name)
  end
end
