class Api::V1::CountriesController < ApplicationController
  # protect_from_forgery with: :null_session

  def index
    # abort params.inspect

    if params[:name]
      # abort params[:name].inspect
      @countries = Country.find_by_name(params[:name])

    else
      @countries = Country.all
    end

    render json: @countries
  end
end
